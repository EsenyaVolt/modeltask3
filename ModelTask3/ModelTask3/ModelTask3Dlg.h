
// ModelTask3Dlg.h : ���� ���������
//

#pragma once

#include <cmath>
#include <vector>

using namespace std;

// ���������� ���� CModelTask3Dlg
class CModelTask3Dlg : public CDialogEx
{
// ��������
public:
	CModelTask3Dlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MODELTASK3_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//������� ���������
	CWnd* PicWndGraph;
	CDC* PicDcGraph;
	CRect PicGraph;

	double Min = -10, Max = 10, Min1 = -10, Max1 = 10, Min2 = -10, Max2 = 10;
	double xx0 = -10, xxmax = 10, yy0 = 0, yymax = 0, xxi = 0, yyi = 0;

	double xpGraph = 0, ypGraph = 0,			//����������� ���������
		xminGraph = -1, xmaxGraph = 1,			//�������������� � ����������� �������� � 
		yminGraph = -0.5, ymaxGraph = 5;			//�������������� � ����������� �������� y
	double mnGraph = -0.1, mxGraph = 1;					//������������ ���������������

	//���������� �����
	CPen osi_pen;		// ����� ��� ����
	CPen setka_pen;		// ��� �����
	CPen graph_pen;		// ��� ������� �������
public:
	afx_msg void OnBnClickedButton1();

	double a_GU;
	double V_param;
	double t_step;
	double eps;
	int k_number;
	double SZ_znach;
	CButton U_x;
	CButton stupenka;

	int scale = 1;
	double defaultX0 = 0.0;
	double defaultY0 = 0.0;
	double E_param = 0.0;
	double PI = 3.1415926;
	char znach[1000];
	vector<double> fi;

	afx_msg void PererisovkaDC(CDC* WinDc, CRect WinPic, CPen* graphpen, double AbsMax);
	afx_msg void PererisovkaDCGraph(vector<double>& solve_buff, CDC* WinDc, CRect WinPic, CPen* graphpen, double AbsMax);
	afx_msg void Mashtab(vector<double>& solve_buff, int dim, double* mmin, double* mmax);
	afx_msg double RK4(int size);
	afx_msg double 	Poten�Func(double x);
	afx_msg double function(double x, double fi);		//�������
	afx_msg void polovin(double a, double b, double x);
	afx_msg double fiR(double E);
};
