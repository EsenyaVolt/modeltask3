﻿
// ModelTask3Dlg.cpp : файл реализации
//

#include "stdafx.h"
#include "ModelTask3.h"
#include "ModelTask3Dlg.h"
#include "afxdialogex.h"

//#include <iostream>
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DOTSGRAPH(x,y) (xpGraph*((x)-xminGraph)),(ypGraph*((y)-ymaxGraph)) // макрос перевода координат для графика сигналa

// диалоговое окно CModelTask3Dlg

CModelTask3Dlg::CModelTask3Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CModelTask3Dlg::IDD, pParent)
	, a_GU(5)
	, V_param(5)
	, t_step(0.1)
	, eps(1e-8)
	, k_number(0)
	, SZ_znach(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CModelTask3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, a_GU);
	DDX_Text(pDX, IDC_EDIT3, V_param);
	DDX_Control(pDX, IDC_RADIO1, U_x);
	DDX_Text(pDX, IDC_EDIT8, t_step);
	DDX_Text(pDX, IDC_EDIT1, eps);
	DDX_Control(pDX, IDC_RADIO2, stupenka);
	DDX_Text(pDX, IDC_EDIT5, k_number);
	DDX_Text(pDX, IDC_EDIT4, SZ_znach);
}

BEGIN_MESSAGE_MAP(CModelTask3Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CModelTask3Dlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// обработчики сообщений CModelTask3Dlg

BOOL CModelTask3Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	//для  картинки
	PicWndGraph = GetDlgItem(IDC_GRAPH);
	PicDcGraph = PicWndGraph->GetDC();
	PicWndGraph->GetClientRect(&PicGraph);

	// перья
	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		1,						//толщина 1 пиксель
		RGB(71, 74, 81));			//цвет  grey

	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 3 пикселя
		RGB(255, 255, 255));			//цвет white

	graph_pen.CreatePen(			//график исходного сигнала
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет blue

	//fazagraph_pen.CreatePen(			//график восстановленного сигнала
	//	PS_SOLID,				//сплошная линия
	//	2,						//толщина 2 пикселя
	//	RGB(255, 0, 0));		//цвет red

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CModelTask3Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PererisovkaDC(PicDcGraph, PicGraph, &graph_pen, 200);
	}
}

void CModelTask3Dlg::Mashtab(vector<double>& solve_buff, int dim, double* mmin, double* mmax)		//определяем функцию масштабирования
{
	*mmin = *mmax = solve_buff[0];

	for (int i = 0; i < dim; i++)
	{
		if (*mmin > solve_buff[i]) *mmin = solve_buff[i];
		if (*mmax < solve_buff[i]) *mmax = solve_buff[i];
	}
}

void CModelTask3Dlg::PererisovkaDC(CDC* WinDc, CRect WinxmaxGraphc, CPen* graphpen, double AbsMax)
{
	//ГРАФИК СИГНАЛА

	//область построения
	xminGraph = -AbsMax * 0.08;			//минимальное значение х
	xmaxGraph = AbsMax * 1.25;			//максимальное значение х
	yminGraph = -Max1 * 0.1;			//минимальное значение y
	ymaxGraph = Max1 * 1.15;		//максимальное значение y

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width() * scale;
	double heightY = WinxmaxGraphc.Height() * scale;
	xpGraph = (widthX / (xmaxGraph - xminGraph));			//Коэффициенты пересчёта координат по Х
	ypGraph = -(heightY / (ymaxGraph - yminGraph));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	MemDc->SelectObject(&osi_pen);

	//создаём Ось Y
	MemDc->MoveTo(DOTSGRAPH(defaultX0, ymaxGraph));
	MemDc->LineTo(DOTSGRAPH(defaultX0, yminGraph));
	//создаём Ось Х
	MemDc->MoveTo(DOTSGRAPH(xminGraph, -defaultY0));
	MemDc->LineTo(DOTSGRAPH(xmaxGraph, -defaultY0));

	MemDc->SelectObject(&setka_pen);

	//отрисовка сетки по y
	for (double x = xminGraph; x <= xmaxGraph; x += xmaxGraph / scale / 8)
	{
		if (x != 0) {
			MemDc->MoveTo(DOTSGRAPH(x, ymaxGraph));
			MemDc->LineTo(DOTSGRAPH(x, yminGraph));
		}
	}
	//отрисовка сетки по x
	for (double y = yminGraph; y <= ymaxGraph; y += ymaxGraph / scale / 4)
	{
		if (y != 0) {
			MemDc->MoveTo(DOTSGRAPH(xminGraph, y));
			MemDc->LineTo(DOTSGRAPH(xmaxGraph, y));
		}
	}

	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	MemDc->SetTextColor(RGB(255, 255, 255));

	// установка шрифта
	CFont font;
	font.CreateFontW(16, 0, 0, 0, FW_HEAVY, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Century Gothic"));
	MemDc->SelectObject(&font);

	//подпись осей
	MemDc->TextOutW(DOTSGRAPH(5, ymaxGraph - 0.03), _T("φ"));	//Y
	MemDc->TextOutW(DOTSGRAPH(xmaxGraph - 15, 0.1), _T("х"));		//X

	//по Y с шагом 5
	for (double i = 0; i <= ymaxGraph; i += ymaxGraph / scale / 8)
	{
		CString str;
		if (i != 0)
		{
			str.Format(_T("%.2f"), i / scale + defaultY0 / scale);
			MemDc->TextOutW(DOTSGRAPH(defaultX0 + xminGraph / 2, i + 0.03 * ymaxGraph), str);
		}
	}
	//по X с шагом 0.5
	for (double j = xminGraph; j <= xmaxGraph; j += xmaxGraph / scale / 8)
	{
		double o = 0.0 / scale;
		CString str;
		if (j != o) {
			str.Format(_T("%.0f"), j / scale - defaultX0 / scale);
			MemDc->TextOutW(DOTSGRAPH(j - xmaxGraph / 100, -defaultY0 - 0.02), str);
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CModelTask3Dlg::PererisovkaDCGraph(vector<double>& solve_buff, CDC* WinDc, CRect WinxmaxGraphc, CPen* graphpen, double AbsMax)
{
	//ГРАФИК СИГНАЛА
	if (U_x.GetCheck() == BST_CHECKED)
	{
		//область построения
		xminGraph = -a_GU * 1.15;			//минимальное значение х
		xmaxGraph = a_GU * 1.15;			//максимальное значение х

		yminGraph = Min1 * 1.05;			//минимальное значение y
		ymaxGraph = Max1 * 2;		//максимальное значение y
	}
	if (stupenka.GetCheck() == BST_CHECKED)
	{
		xminGraph = -V_param * 1.2;
		xmaxGraph = V_param * 0.3;
		yminGraph = Min1;			//минимальное значение y
		ymaxGraph = Max1;
	}

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width() * scale;
	double heightY = WinxmaxGraphc.Height() * scale;
	xpGraph = (widthX / (xmaxGraph - xminGraph));			//Коэффициенты пересчёта координат по Х
	ypGraph = -(heightY / (ymaxGraph - yminGraph));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	
	MemDc->SelectObject(&setka_pen);

	//отрисовка сетки по y
	for (double x = xminGraph; x <= xmaxGraph; x += xmaxGraph / scale / 4)
	{
		if (x != 0) {
			MemDc->MoveTo(DOTSGRAPH(x, ymaxGraph));
			MemDc->LineTo(DOTSGRAPH(x, yminGraph));
		}
	}
	//отрисовка сетки по x
	for (double y = yminGraph; y <= ymaxGraph; y += ymaxGraph / scale / 4)
	{
		if (y != 0) {
			MemDc->MoveTo(DOTSGRAPH(xminGraph, y));
			MemDc->LineTo(DOTSGRAPH(xmaxGraph, y));
		}
	}

	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	MemDc->SetTextColor(RGB(255, 255, 255));

	// установка шрифта
	CFont font;
	font.CreateFontW(16, 0, 0, 0, FW_HEAVY, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Century Gothic"));

	//по Y с шагом 5
	for (double i = yminGraph; i <= ymaxGraph; i++)
	{
		MemDc->SelectObject(&font);
		CString str;
		if ((int)i != 0.)
		{
			str.Format(_T("%.0f"), (int)i / scale + defaultY0 / scale);
			MemDc->TextOutW(DOTSGRAPH(defaultX0 - xmaxGraph * 0.05, (int)i), str);

			MemDc->SelectObject(&osi_pen);
			MemDc->MoveTo(DOTSGRAPH(xmaxGraph * 0.01, (int)i));
			MemDc->LineTo(DOTSGRAPH(-xmaxGraph * 0.01, (int)i));
		}
	}
	//по X с шагом 0.5
	for (double j = xminGraph; j <= xmaxGraph; j++)
	{
		MemDc->SelectObject(&font);
		double o = 0.0 / scale;
		CString str;
		if ((int)j != o) {
			str.Format(_T("%.0f"), (int)j / scale - defaultX0 / scale);
			MemDc->TextOutW(DOTSGRAPH((int)j, defaultY0 - ymaxGraph * 0.02), str);

			MemDc->SelectObject(&osi_pen);
			MemDc->MoveTo(DOTSGRAPH((int)j, ymaxGraph * 0.03));
			MemDc->LineTo(DOTSGRAPH((int)j, -ymaxGraph * 0.03));
		}
	}

	if (U_x.GetCheck() == BST_CHECKED)
	{
		MemDc->SelectObject(&osi_pen);

		//создаём Ось Y
		MemDc->MoveTo(DOTSGRAPH(defaultX0, ymaxGraph));
		MemDc->LineTo(DOTSGRAPH(defaultX0, yminGraph));
		//создаём Ось Х
		MemDc->MoveTo(DOTSGRAPH(xminGraph, -defaultY0));
		MemDc->LineTo(DOTSGRAPH(xmaxGraph, -defaultY0));

		//подпись осей
		MemDc->TextOutW(DOTSGRAPH(xmaxGraph * 0.05, ymaxGraph * 0.95), _T("U"));	//Y
		MemDc->TextOutW(DOTSGRAPH(xmaxGraph * 0.95, ymaxGraph * 0.15), _T("х"));		//X

		// отрисовка
		MemDc->SelectObject(graphpen);
		xxi = -a_GU;
		MemDc->MoveTo(DOTSGRAPH(xxi, solve_buff[0] * scale - defaultY0));
		for (int i = 0; i < AbsMax; i++)
		{
			MemDc->LineTo(DOTSGRAPH(xxi, solve_buff[i] * scale - defaultY0));
			xxi += t_step;
		}
	}

	if (stupenka.GetCheck() == BST_CHECKED)
	{
		//создаём Ось Y
		MemDc->MoveTo(DOTSGRAPH(defaultX0, ymaxGraph));
		MemDc->LineTo(DOTSGRAPH(defaultX0, yminGraph));
		//создаём Ось Х
		MemDc->MoveTo(DOTSGRAPH(xminGraph, -defaultY0));
		MemDc->LineTo(DOTSGRAPH(xmaxGraph, -defaultY0));
	
		//подпись осей
		MemDc->TextOutW(DOTSGRAPH(xmaxGraph * 0.05, ymaxGraph * 0.95), _T("φ"));	//Y
		MemDc->TextOutW(DOTSGRAPH(xmaxGraph * 0.95, ymaxGraph * 0.15), _T("ε"));		//X

		// отрисовка
		MemDc->SelectObject(graphpen);
		MemDc->MoveTo(DOTSGRAPH(-V_param, solve_buff[0] * scale - defaultY0));
		int iter = 0;
		for (double i = -V_param; i <= 1; i += t_step)
		{
			MemDc->LineTo(DOTSGRAPH(i, solve_buff[iter] * scale - defaultY0));
			iter++;
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CModelTask3Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double CModelTask3Dlg::PotenсFunc(double x)
{
	if (x > -a_GU / 2 && x < a_GU / 2)
	{
		return -V_param;
	}
	else
	{
		return 0;
	}
}

double CModelTask3Dlg::function(double x, double fi)
{
	return -((E_param + PotenсFunc(x)) * cos(fi) * cos(fi) + sin(fi) * sin(fi));
}

double CModelTask3Dlg::fiR(double E)
{
	int size = a_GU / t_step;

	for (double i = -E; i <= 1; i+= t_step)
	{
		E_param = i;
		fi.push_back(RK4(size));
	}
	
	return fi.back();
}

void CModelTask3Dlg::polovin(double a, double b, double x) //функция, описывающая метод половинного деления
{
	x = 0;
	double dobavochka = (2 * k_number + 1) * PI / 2;

	//if ((fiR(a) + dobavochka) * (fiR(b) + dobavochka) >= 0 || a > b) //проверяем условие, если оно правильное то, промежуток неверный
	//{
	//	MessageBox(L"Не выполняются условия", L"Error", MB_OK);
	//}

	while (fabs(b - a) >= eps) //ищем значение, которое отличается от истинного на eps
	{
		x = (a + b) / 2; //нахождение середины отрезка, приближение корня
		if ((fiR(a) + dobavochka) * (fiR(x) + dobavochka) < 0) b = x;
		else a = x;
	}
}

double CModelTask3Dlg::RK4(int size)
{
	vector<double> k1;		k1.clear();		k1.resize(size + 1);	// 
	vector<double> k2;		k2.clear();		k2.resize(size + 1);	// массив коэффициентов Р-К
	vector<double> k3;		k3.clear();		k3.resize(size + 1);	// 
	vector<double> k4;		k4.clear();		k4.resize(size + 1);	//
	vector<double> y;		y.clear();		y.resize(size + 1);

	y[0] = 0;

	int iter = 0;

	for (double i = -a_GU/2; i <= a_GU / 2; i+= t_step)
	{
		k1[iter] = function(i, y[iter]);
		k2[iter] = function(i + t_step / 2, y[iter] + (k1[iter] * t_step / 2));
		k3[iter] = function(i + t_step / 2, y[iter] + (k2[iter] * t_step / 2));
		k4[iter] = function(i / 2 +  t_step, y[iter] + k3[iter] * t_step);

		y[iter + 1] = y[iter] + (k1[iter] + 2 * k2[iter] + 2 * k3[iter] + k4[iter]) * t_step / 6;
		iter++;
	}

	return y[iter];
}

void CModelTask3Dlg::OnBnClickedButton1()
{
	UpdateData(TRUE);

	if (U_x.GetCheck() == BST_CHECKED)
	{
		vector<double> func;
		func.clear();

		ofstream out("table.txt");
		for (double i = -a_GU; i < a_GU; i += t_step)
		{
			func.push_back(PotenсFunc(i));
			out << i << "\t\t" << PotenсFunc(i) << endl;
		}
		out.close();

		Min1 = -V_param * 1.15;
		Max1 = V_param * 0.3;

		PererisovkaDCGraph(func, PicDcGraph, PicGraph, &graph_pen, func.size());
	}

	if (stupenka.GetCheck() == BST_CHECKED)
	{
		fi.clear();
		fiR(V_param);

		//Mashtab(fi, fi.size(), &Min1, &Max1);
		Min1 = -0.5;
		Max1 = 1.5;
		PererisovkaDCGraph(fi, PicDcGraph, PicGraph, &graph_pen, fi.size());

		
		polovin(-V_param, 1, SZ_znach);
	}

	UpdateData(FALSE);
	//Mashtab(solve_buff, AbsMax, &Min1, &Max1);
	//https://drive.google.com/drive/folders/1ty4mBsjkS3tOPPcr1oitglm5wNBe3A4F
}
